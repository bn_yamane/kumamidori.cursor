Kumamidori.Cursor
=======

[![Build Status](https://travis-ci.org/kumamidori/Kumamidori.Cursor.svg?branch=master)](https://travis-ci.org/kumamidori/Kumamidori.Cursor)

Minimum Cursor
------------

純粋なページネーションとは違った、ごくシンプルなカーソル。
バッチ処理の用途でよく見るので残すことにした。

Example
------------


```

      $config = ['min' => 100, 'max' => 300, 'offset' => 100];
      $cursor = new Cursor($config);
      foreach ($cursor as $pos => $current) {
          echo var_export($current, true) . PHP_EOL;
      }

```

* 出力結果

```

   [  'start' => 100,  'end' => 199,  'pos_page' => 1,]
   [  'start' => 200,  'end' => 299,  'pos_page' => 2,]
   [  'start' => 300,  'end' => 300,  'pos_page' => 3,]

```

Requirements
------------
 * PHP 5.3+
