<?php

use Kumamidori\Cursor\Cursor;

require realpath(__DIR__ . '/../vendor/autoload.php');

$config = array('min' => 100, 'max' => 300, 'offset' => 100);

$cursor = new Cursor($config);

foreach ($cursor as $pos => $current) {
    echo var_export($current, true) . PHP_EOL;
}
