<?php

namespace Kumamidori\Cursor;

class CursorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider correctConfigProvider
     */
    public function testCalcPageNumByReturnsCorrectPageNum($expected, $config)
    {
        $cursor = new Cursor($config);

        $this->assertSame($expected, $cursor->getPageNum());
    }

    /**
     * @dataProvider invalidConfigProvider
     * @expectedException \InvalidArgumentException
     */
    public function testGuardInvalidThrowsInvalidArgumentExceptionCaseInvalid($config)
    {
        new Cursor($config);
    }

    /**
     * @dataProvider invalidRelationConfigProvider
     * @expectedException \LogicException
     */
    public function testGuardInvalidThrowsLogicExceptionCaseRelationInvalid($config)
    {
        new Cursor($config);
    }

    public function testRewindSetStartCaseFirstPage()
    {
        $fixture = array('min' => 2, 'max' => 11, 'offset' => 4);
        $cursor = new Cursor($fixture);

        $cursor->rewind();

        $this->assertSame(2, $cursor->getStart());
    }

    public function testRewindSetStartCaseLastPage()
    {
        $fixture = array('min' => 2, 'max' => 4, 'offset' => 2);
        $cursor = new Cursor($fixture);

        $cursor->rewind();

        $this->assertSame(2, $cursor->getStart());
    }

    public function testRewindSetEndCaseFirstPage()
    {
        $fixture = array('min' => 2, 'max' => 11, 'offset' => 4);
        $cursor = new Cursor($fixture);

        $cursor->rewind();

        $this->assertSame(5, $cursor->getEnd());
    }

    public function testCurrentReturnsCursorParams()
    {
        $fixture = array('min' => 2, 'max' => 11, 'offset' => 4);
        $cursor = new Cursor($fixture);
        $actual = $cursor->current();

        $this->assertSame(2, $actual['start']);
        $this->assertSame(5, $actual['end']);
        $this->assertSame(1, $actual['pos_page']);
    }

    public function testNextReturnsCursorParams()
    {
        $fixture = array('min' => 2, 'max' => 11, 'offset' => 4);
        $cursor = new Cursor($fixture);
        $cursor->next();

        $this->assertSame(6, $cursor->getStart());
        $this->assertSame(9, $cursor->getEnd());
        $this->assertSame(2, $cursor->getPosPage());
        $this->assertSame(2, $cursor->key());
    }

    public function testCalcEndReturnsConfigMaxCaseLastPage()
    {
        $fixture = array('min' => 2, 'max' => 11, 'offset' => 4);
        $cursor = new Cursor($fixture);
        // 6-9
        $cursor->next();
        // 10-11
        $cursor->next();

        $this->assertSame(10, $cursor->getStart());
        $this->assertSame(11, $cursor->getEnd());
        $this->assertSame(3, $cursor->key());
    }

    public function testValidReturnsFalseCaseInvalid()
    {
        $fixture = array('min' => 0, 'max' => 1, 'offset' => 4);
        $cursor = new Cursor($fixture);
        $cursor->next();

        $this->assertFalse($cursor->valid());
    }

    public function testForeachCaseIndivisible()
    {
        // page1:[100-199], page2:[200-299], page3:[300-300]
        $fixture = array('min' => 100, 'max' => 300, 'offset' => 100);

        $cursor = $this->getMock('\Kumamidori\Cursor\Cursor', array('current'), array($fixture));
        $cursor->expects($this->exactly(3))
            ->method('current')
            ->will($this->returnValue(true));

        foreach ($cursor as $pos => $current) {
        }
    }

    public function testForeachCaseDivisible()
    {
        // page1:[0-1], page2:[2-3]
        $fixture = array('min' => 0, 'max' => 3, 'offset' => 2);

        $cursor = $this->getMock('\Kumamidori\Cursor\Cursor', array('current'), array($fixture));
        $cursor->expects($this->exactly(2))
            ->method('current')
            ->will($this->returnValue(true));

        foreach ($cursor as $pos => $current) {
        }
    }

    public function testForeachCaseDivisibleOffset1()
    {
        // page1:[0], page2:[1]
        $fixture = array('min' => 0, 'max' => 1, 'offset' => 1);

        $cursor = $this->getMock('\Kumamidori\Cursor\Cursor', array('current'), array($fixture));
        $cursor->expects($this->exactly(2))
            ->method('current')
            ->will($this->returnValue(true));

        foreach ($cursor as $pos => $current) {
        }
    }

    public function correctConfigProvider()
    {
        // 期待するページ数、入力の初期設定配列
        return array(
            // 0オリジンも想定
            array(1, array('min' => 0, 'max' => 0, 'offset' => 4)),
            array(1, array('min' => 0, 'max' => 1, 'offset' => 4)),
            // 以下は1オリジン
            array(1, array('min' => 1, 'max' => 1, 'offset' => 4)),
            array(1, array('min' => 1, 'max' => 2, 'offset' => 4)),
            array(2, array('min' => 1, 'max' => 5, 'offset' => 4)),
            array(2, array('min' => 3, 'max' => 6, 'offset' => 2)),
            array(3, array('min' => 3, 'max' => 7, 'offset' => 2)),
        );
    }

    public function invalidConfigProvider()
    {
        return array(
            array(array()),
            array(array('min' => 1)),
            array(array('offset' => 0)),
            array(array('min' => 'a', 'max' => 0, 'offset' => 4)),
        );
    }

    public function invalidRelationConfigProvider()
    {
        return array(
            array(array('min' => 2, 'max' => 1)),
            array(array('min' => -1, 'max' => 1)),
        );
    }
}
