<?php
/**
 * This file is part of the Kumamidori.Cursor
 *
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */
namespace Kumamidori\Cursor;

/**
 * Kumamidori.Cursor
 *
 * 純粋なページネーションとは違った、ごくシンプルなカーソル。
 *
 * 同じような機能を2,3回見ていて嫌なので残すことにした。
 * PHP5.3でも動くようにしてある。
 *
 * @example
 *
 *      $config = array('min' => 100, 'max' => 300, 'offset' => 100);
 *      $cursor = new Cursor($config);
 *      foreach ($cursor as $pos => $current) {
 *          echo var_export($current, true) . PHP_EOL;
 *      }
 *
 * 出力結果
 * array (  'start' => 100,  'end' => 199,  'pos_page' => 1,)
 * array (  'start' => 200,  'end' => 299,  'pos_page' => 2,)
 * array (  'start' => 300,  'end' => 300,  'pos_page' => 3,)
 */
class Cursor implements \Iterator
{
    private $min;
    private $max;
    private $offset;

    /**
     * @var int 今みているページ番号
     */
    private $pos_page;
    /**
     * @var int 全ページ数
     */
    private $page_num;

    private $start;
    private $end;

    /**
     * @param array $config
     *
     * example:
     * from 100 to 500, offset 200 -> page1. 100-299, page2. 300-399, page3. 400-500. total 3pages.
     *
     *      [
     *          'min' => 100,
     *          'max' => 500,
     *          'offset' => 200
     *      ]
     *
     * @throws \InvalidArgumentException
     * @throws \LogicException
     */
    public function __construct(array $config = array())
    {
        $this->guardInvalid($config);

        $this->min = $config['min'];
        $this->max = $config['max'];
        $this->offset = $config['offset'];
        $this->page_num = $this->calcPageNumBy($config['min'], $config['max'], $config['offset']);
        $this->rewind();
    }

    private function calcPageNumBy($min, $max, $offset)
    {
        if ((($max - $min + 1) % $offset) === 0) {
            // 1,2,3 で offset が 3 -> 1
            $page_num = ($max - $min + 1) / $offset;
        } else {
            // 1,2,3 で offset が 2
            $page_num = (int)(ceil(($max - $min + 1) / $offset));
        }
        return (int) $page_num;
    }

    /**
     * @inheritdoc
     */
    public function current()
    {
        return array('start' => $this->getStart(), 'end' => $this->getEnd(), 'pos_page' => $this->getPosPage());
    }

    /**
     * @inheritdoc
     */
    public function next()
    {
        $this->pos_page++;
        $this->start = $this->getEnd() + 1;
        $this->end = $this->calcEnd();
    }

    /**
     * @inheritdoc
     */
    public function key()
    {
        return $this->getPosPage();
    }

    /**
     * @inheritdoc
     */
    public function valid()
    {
        return $this->getPosPage() <= $this->getPageNum();
    }

    /**
     * @inheritdoc
     */
    public function rewind()
    {
        // 現在指している位置
        $this->pos_page = 1;

        $this->start = $this->min;
        $this->end = $this->calcEnd();
    }

    private function calcEnd()
    {
        if ($this->getPosPage() >= $this->getPageNum()) {

            return $this->getMax();
        }
        return $this->getStart() + $this->getOffset() - 1;
    }

    private function guardInvalid($config)
    {
        if (!$config || !isset($config['min']) || !isset($config['max']) || !isset($config['offset'])) {
            throw new \InvalidArgumentException(var_export($config, true));
        }
        if (empty($config['offset'])) {
            throw new \InvalidArgumentException(var_export($config, true));
        }
        if (!is_numeric($config['min']) || !is_numeric($config['max']) || !is_numeric($config['offset'])) {
            throw new \InvalidArgumentException(var_export($config, true));
        }
        if ($config['min'] < 0) {
            throw new \LogicException('"min" must be equal or greater than 0. ' . var_export($config, true));
        }
        if ($config['min'] > $config['max']) {
            throw new \LogicException(var_export($config, true));
        }
    }

    public function getStart()
    {
        return $this->start;
    }

    public function getEnd()
    {
        return $this->end;
    }

    public function getPosPage()
    {
        return $this->pos_page;
    }

    public function getPageNum()
    {
        return $this->page_num;
    }

    public function getMax()
    {
        return $this->max;
    }

    public function getMin()
    {
        return $this->min;
    }

    public function getOffset()
    {
        return $this->offset;
    }
}
