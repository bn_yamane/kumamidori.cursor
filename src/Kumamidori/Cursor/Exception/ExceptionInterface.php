<?php
/**
 * This file is part of the Kumamidori.Cursor
 *
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */
namespace Kumamidori\Cursor\Exception;

/**
 * Generic package exception
 *
 * @package Kumamidori.Cursor
 */
interface ExceptionInterface
{
}
