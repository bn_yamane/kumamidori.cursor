<?php
/**
 * This file is part of the Kumamidori.Cursor
 *
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */
namespace Kumamidori\Cursor\Exception;

/**
 * LogicException
 *
 * @package Kumamidori.Cursor
 */
class LogicException extends \LogicException implements ExceptionInterface
{
}
