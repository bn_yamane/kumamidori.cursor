<?php
/**
 * This file is part of the Kumamidori.Cursor
 *
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */
namespace Kumamidori\Cursor\Exception;

/**
 * RuntimeException
 *
 * @package Kumamidori.Cursor
 */
class RuntimeException extends \LogicException implements ExceptionInterface
{
}
